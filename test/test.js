const { factorial, div_check } = require ('../src/util.js');
const { expect, assert } = require('chai');


describe('test_fun_factorials_test_suite', () => {
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	});

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		expect(product).to.equal(1);
	});

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		expect(product).to.equal(1);
	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);
	});
})

describe('test_divisibility_checks_test_suite', () => {
	it('test_100_is_divisible_by_5', () => {
		expect(div_check(100)).to.equal(true);
	});

	it('test_49_is_divisible_by_7', () => {
		expect(div_check(49)).to.equal(true);
	});

	it('test_30_is_divisible_by_5', () => {
		expect(div_check(30)).to.equal(true);
	});

	it('test_56_is_divisible_by_7', () => {
		expect(div_check(30)).to.equal(true);
	});
})